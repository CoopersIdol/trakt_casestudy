package com.pkoerfer.traktcasestudy.util;

import android.support.annotation.NonNull;

import com.pkoerfer.traktcasestudy.model.tmdb.MovieImagedataObject;

/**
 * Created by Pascal on 14.10.2016.
 */
public interface TmdbLoadListener {
    void onImagesLoaded(@NonNull final MovieImagedataObject images);
}
