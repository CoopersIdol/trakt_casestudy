package com.pkoerfer.traktcasestudy.rest;

import android.support.annotation.NonNull;
import android.util.Log;

import com.pkoerfer.traktcasestudy.model.MovieDataObject;
import com.pkoerfer.traktcasestudy.model.MovieSearchObject;
import com.pkoerfer.traktcasestudy.util.LoadMoviesListener;
import com.pkoerfer.traktcasestudy.util.SearchTermListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * class responsible for dispatching the trakt API calls
 * Created by Pascal on 13.10.2016.
 */
public class RestHandler {

    private static RestHandler _instance;

    public static RestHandler getInstance() {
        if (_instance == null) {
            _instance = new RestHandler();
        }
        return _instance;
    }

    private TraktService mTraktService;

    private Call<List<MovieSearchObject>> queryCall = null;

    public void init(RestConfig config) {
        mTraktService = config.getRetrofit().create(TraktService.class);
    }

    /**
     * dispatch loading of the popular movies
     * @param loadMoviesListener the listener to be called when data has been received
     * @param page the page to load
     */
    public void getPopularMovies(@NonNull final LoadMoviesListener loadMoviesListener, Integer page) {
        Call<List<MovieDataObject>> movies = mTraktService.getPopularMovies(RestConfig.TRAKT_API_VERSION, RestConfig.TRAKT_CLIENT_ID, page, RestConfig.TRAKT_PAGINATION_LIMIT);
        movies.enqueue(new Callback<List<MovieDataObject>>() {
            @Override
            public void onResponse(Call<List<MovieDataObject>> call, Response<List<MovieDataObject>> response) {
                if (response.body() != null) {
                    Log.d(getClass().getName(), response.raw().headers().get("x-pagination-page"));
                    Integer currentPage = Integer.parseInt(response.raw().headers().get("x-pagination-page"));
                    loadMoviesListener.onLoadingFinished(response.body(), currentPage);
                }
            }

            @Override
            public void onFailure(Call<List<MovieDataObject>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    /**
     * dispatch loading of a query with the given searchTerm
     * @param searchTermListener listener to be called when finished loading
     * @param searchTerm the term to be searched for
     * @param page the page to be loaded
     */
    public void getQueryResults(@NonNull final SearchTermListener searchTermListener, @NonNull final String searchTerm, Integer page) {
        if (queryCall != null) {
            queryCall.cancel();
        }
        queryCall = mTraktService.getSearchResults(RestConfig.TRAKT_API_VERSION, RestConfig.TRAKT_CLIENT_ID, searchTerm, page, RestConfig.TRAKT_PAGINATION_LIMIT);
        queryCall.enqueue(new Callback<List<MovieSearchObject>>() {
            @Override
            public void onResponse(Call<List<MovieSearchObject>> call, Response<List<MovieSearchObject>> response) {
                if (response.body() != null) {
                    Integer currentPage = Integer.parseInt(response.raw().headers().get("x-pagination-page"));
                    searchTermListener.onSearchResultReceived(response.body(), currentPage);
                }
                queryCall = null;
            }

            @Override
            public void onFailure(Call<List<MovieSearchObject>> call, Throwable t) {
                queryCall = null;
                Log.e("Retrofit!!!", t.getMessage());
            }
        });
    }
}
