package com.pkoerfer.traktcasestudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 13.10.2016.
 */

public class MovieDataObject extends BaseDataObject{

    @SerializedName("ids")
    private IdDataObject ids;

    public MovieDataObject(String title, Integer year, IdDataObject ids) {
        super(title, year);
        this.ids = ids;
    }

    public IdDataObject getIds() {
        return ids;
    }

    @Override
    public String toString() {
        return String.format("Movie %s released in %s", getTitle(), getYear());
    }
}
