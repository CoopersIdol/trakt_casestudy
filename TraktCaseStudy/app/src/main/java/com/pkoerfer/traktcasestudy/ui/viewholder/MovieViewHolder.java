package com.pkoerfer.traktcasestudy.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.pkoerfer.traktcasestudy.util.Loca;
import com.pkoerfer.traktcasestudy.model.MovieDataObject;
import com.pkoerfer.traktcasestudy.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pascal on 13.10.2016.
 */

public class MovieViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.movieCell_title_textView)
    TextView mTitleTV;

    @BindView(R.id.movieCell_year_textView)
    TextView mYearTV;

    public MovieViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setMovieDetails(MovieDataObject movie) {
        if (movie != null) {
            mTitleTV.setText(movie.getTitle());
            mYearTV.setText(Loca.getStringWithParamter(mYearTV.getContext(), R.string.year_label, movie.getYear()));
        }
    }
}
