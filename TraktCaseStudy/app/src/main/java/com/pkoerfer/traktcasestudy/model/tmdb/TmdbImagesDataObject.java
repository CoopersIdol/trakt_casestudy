package com.pkoerfer.traktcasestudy.model.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 14.10.2016.
 */

public class TmdbImagesDataObject {

    @SerializedName("base_url")
    private String baseUrl;

    @SerializedName("secure_base_url")
    private String secureBaseUrl;

    @SerializedName("backdrop_sizes")
    private String[] backdropSizes;

    @SerializedName("logo_sizes")
    private String[] logoSizes;

    @SerializedName("poster_sizes")
    private String[] posterSizes;

    @SerializedName("profile_sizes")
    private String[] profileSizes;

    @SerializedName("still_sizes")
    private String[] stillSizes;

    public TmdbImagesDataObject(String baseUrl, String secureBaseUrl, String[] backdropSizes, String[] logoSizes, String[] posterSizes, String[] profileSizes, String[] stillSizes) {
        this.baseUrl = baseUrl;
        this.secureBaseUrl = secureBaseUrl;
        this.backdropSizes = backdropSizes;
        this.logoSizes = logoSizes;
        this.posterSizes = posterSizes;
        this.profileSizes = profileSizes;
        this.stillSizes = stillSizes;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getSecureBaseUrl() {
        return secureBaseUrl;
    }

    public String[] getBackdropSizes() {
        return backdropSizes;
    }

    public String[] getLogoSizes() {
        return logoSizes;
    }

    public String[] getPosterSizes() {
        return posterSizes;
    }

    public String[] getProfileSizes() {
        return profileSizes;
    }

    public String[] getStillSizes() {
        return stillSizes;
    }
}
