package com.pkoerfer.traktcasestudy.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.pkoerfer.traktcasestudy.R;
import com.pkoerfer.traktcasestudy.model.MovieSearchObject;
import com.pkoerfer.traktcasestudy.ui.viewholder.QueryResultViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * adapter class responsible for updating the view holders when new data has been added
 * Created by Pascal on 14.10.2016.
 */
public class QueryResultAdapter extends RecyclerView.Adapter<QueryResultViewHolder> {

    private List<MovieSearchObject> mQueryResults = new ArrayList<>();

    @Override
    public QueryResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QueryResultViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.gridcell_query_result, parent, false));
    }

    @Override
    public void onBindViewHolder(QueryResultViewHolder holder, int position) {
        holder.displayMovieOverview(mQueryResults.get(position));
    }

    /**
     * fill the adapter with data and dispatch a new draw
     * @param queryResults the data to be set
     */
    public void setData(@NonNull List<MovieSearchObject> queryResults) {
        if (mQueryResults == null) {
            mQueryResults = new ArrayList<>();
        }
        mQueryResults.addAll(queryResults);
        notifyDataSetChanged();
    }

    /**
     * clear the adapter and force an empty draw
     */
    public void clear() {
        mQueryResults = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mQueryResults != null) {
            return mQueryResults.size();
        }
        return 0;
    }
}
