package com.pkoerfer.traktcasestudy.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.pkoerfer.traktcasestudy.model.MovieDataObject;
import com.pkoerfer.traktcasestudy.R;
import com.pkoerfer.traktcasestudy.ui.viewholder.MovieViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * adapter class responsible for updating the view holders when new data has been added
 * Created by Pascal on 13.10.2016.
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private List<MovieDataObject> mMovies = new ArrayList<>();

    public void addMovies(List<MovieDataObject> movies) {
        if (mMovies == null) {
            mMovies = new ArrayList<>();
        }
        mMovies.addAll(movies);
        notifyDataSetChanged();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_movie, parent, false));
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.setMovieDetails(mMovies.get(position));
    }

    public void clear() {
        mMovies = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mMovies != null) {
            return mMovies.size();
        }
        return 0;
    }
}
