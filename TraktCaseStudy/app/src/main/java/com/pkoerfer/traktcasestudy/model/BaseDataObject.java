package com.pkoerfer.traktcasestudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 14.10.2016.
 */
public class BaseDataObject {

    @SerializedName("title")
    private String title;

    @SerializedName("year")
    private Integer year;

    public BaseDataObject(String title, Integer year) {
        this.title = title;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }
}
