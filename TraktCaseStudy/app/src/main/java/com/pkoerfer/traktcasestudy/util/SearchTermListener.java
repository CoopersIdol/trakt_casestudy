package com.pkoerfer.traktcasestudy.util;

import android.support.annotation.NonNull;

import com.pkoerfer.traktcasestudy.model.MovieSearchObject;

import java.util.List;

/**
 * Created by Pascal on 14.10.2016.
 */

public interface SearchTermListener {
    void onSearchResultReceived(@NonNull final List<MovieSearchObject> movies, @NonNull final Integer currentPage);
}
