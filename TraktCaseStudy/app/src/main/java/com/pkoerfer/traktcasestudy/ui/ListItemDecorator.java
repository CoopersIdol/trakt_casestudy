package com.pkoerfer.traktcasestudy.ui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.pkoerfer.traktcasestudy.R;

/**
 * class responsible for customizing the {@link RecyclerView} items margin
 * Created by Pascal on 16.10.2016.
 */
public class ListItemDecorator extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int margin = (int) view.getContext().getResources().getDimension(R.dimen.margin_small);
        outRect.set(margin, 0, margin, margin);
    }
}
