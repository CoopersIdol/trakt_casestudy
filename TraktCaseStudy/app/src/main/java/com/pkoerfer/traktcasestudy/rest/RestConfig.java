package com.pkoerfer.traktcasestudy.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * class responsible for creating the Trakt retrofit configuration
 * Created by Pascal on 13.10.2016.
 */
public class RestConfig {

    private Retrofit mRetrofit;

    public static final String TRAKT_CLIENT_ID = "ad005b8c117cdeee58a1bdb7089ea31386cd489b21e14b19818c91511f12a086";
    public static final String TRAKT_API_VERSION = "2";

    public static final Integer TRAKT_PAGINATION_LIMIT = 10;

    public RestConfig() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl("https://api.trakt.tv")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }
}
