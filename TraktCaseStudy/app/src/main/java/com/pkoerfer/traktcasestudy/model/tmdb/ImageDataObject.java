package com.pkoerfer.traktcasestudy.model.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 15.10.2016.
 */

public class ImageDataObject {

    @SerializedName("aspect_ratio")
    private Double aspectRatio;

    @SerializedName("file_path")
    private String filePath;

    @SerializedName("height")
    private Integer height;

    @SerializedName("iso_639_1")
    private String iso639;

    @SerializedName("vote_average")
    private Double voteAverage;

    @SerializedName("vote_count")
    private Long voteCount;

    @SerializedName("width")
    private Integer width;

    public ImageDataObject(Double aspectRatio, String filePath, Integer height, String iso639, Double voteAverage, Long voteCount, Integer width) {
        this.aspectRatio = aspectRatio;
        this.filePath = filePath;
        this.height = height;
        this.iso639 = iso639;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.width = width;
    }

    public Double getAspectRatio() {
        return aspectRatio;
    }

    public String getFilePath() {
        return filePath;
    }

    public Integer getHeight() {
        return height;
    }

    public String getIso639() {
        return iso639;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public Long getVoteCount() {
        return voteCount;
    }

    public Integer getWidth() {
        return width;
    }
}
