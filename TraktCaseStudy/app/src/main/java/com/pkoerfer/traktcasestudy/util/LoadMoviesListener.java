package com.pkoerfer.traktcasestudy.util;

import com.pkoerfer.traktcasestudy.model.MovieDataObject;

import java.util.List;

/**
 * Created by Pascal on 13.10.2016.
 */
public interface LoadMoviesListener {
    void onLoadingFinished(List<MovieDataObject> movies, Integer currentPage);
}
