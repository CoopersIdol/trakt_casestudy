package com.pkoerfer.traktcasestudy.rest;

import com.pkoerfer.traktcasestudy.model.MovieDataObject;
import com.pkoerfer.traktcasestudy.model.MovieSearchObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * interface containing the calls made to the trakt API
 * Created by Pascal on 13.10.2016.
 */
public interface TraktService {
    @GET("movies/popular")
    Call<List<MovieDataObject>> getPopularMovies(@Header("trakt-api-version") String apiVersion, @Header("trakt-api-key") String clientId, @Query("page") Integer page, @Query("limit") Integer Limit);

    @GET("/search/movie")
    Call<List<MovieSearchObject>> getSearchResults(@Header("trakt-api-version") String apiVersion, @Header("trakt-api-key") String clientId, @Query("query") String term, @Query("page") Integer page, @Query("limit") Integer Limit);
}