package com.pkoerfer.traktcasestudy.rest.movieDatabase;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.util.Log;

import com.pkoerfer.traktcasestudy.RequestPoolObject;
import com.pkoerfer.traktcasestudy.model.tmdb.MovieImagedataObject;
import com.pkoerfer.traktcasestudy.model.tmdb.TmdbApiConfiguration;
import com.pkoerfer.traktcasestudy.util.TmdbLoadListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * class responsible for dispatching the trakt API calls
 * Created by Pascal on 14.10.2016.
 */
public class TmdbRestHandler {

    private static TmdbRestHandler instance;

    public static TmdbRestHandler getInstance() {
        if (instance == null) {
            instance = new TmdbRestHandler();
        }
        return instance;
    }

    private TmdbService mTmdbService;

    private List<RequestPoolObject> mRequestPool = new ArrayList<>();

    private boolean mConfigLoaded = false;

    private int mAvailableRequests = -1;
    private int mMaxRequests = -1;
    private int mNextCallPossibleIn = -1;

    private static CountDownTimer mTimer = null;

    public void init() {
        mTmdbService = TmdbRestConfig.getInstance().getRetrofit().create(TmdbService.class);
        loadTmdbConfig();
    }

    /**
     * dispatch loading of the TMDB configuration
     */
    private void loadTmdbConfig() {
        Call<TmdbApiConfiguration> configCall = mTmdbService.getApiConfiguration(TmdbRestConfig.API_KEY);
        configCall.enqueue(new Callback<TmdbApiConfiguration>() {
            @Override
            public void onResponse(Call<TmdbApiConfiguration> call, Response<TmdbApiConfiguration> response) {
                if (response.body() != null) {
                    mConfigLoaded = true;
                    mMaxRequests =  Integer.parseInt(response.raw().header("X-RateLimit-Limit"));
                    mAvailableRequests =  Integer.parseInt(response.raw().header("X-RateLimit-Remaining"));
                    Log.d(getClass().getName(), response.body().toString());
                    TmdbRestConfig.getInstance().setApiConfiguration(response.body());
                }
            }

            @Override
            public void onFailure(Call<TmdbApiConfiguration> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    /**
     * dispatch loading of a movies images
     * @param listener the listener to be called when the data has been received
     * @param movieId the movieId to use as search parameter
     */
    public void getMovieImages(@NonNull final TmdbLoadListener listener, @NonNull final Long movieId) {
        if (mConfigLoaded) {
            final Call<MovieImagedataObject> imageCall = mTmdbService.getMovieImages(movieId, TmdbRestConfig.API_KEY);
            if (mAvailableRequests > 0) {
                imageCall.enqueue(getMovieImageCallback(listener, movieId));
            } else {
                RequestPoolObject mPoolObject = new RequestPoolObject(imageCall, listener, movieId);
                if (mRequestPool.size() >= 10) {
                    mRequestPool.remove(0);
                }
                mRequestPool.add(mPoolObject);
            }
        }
    }

    /**
     * create a callback to be informed when call has been finished
     * @param listener the listener to be called when the data has been loaded
     * @param movieId the calls movieId
     * @return the created callback
     */
    private Callback<MovieImagedataObject> getMovieImageCallback(@NonNull final TmdbLoadListener listener, @NonNull final Long movieId) {
        return new Callback<MovieImagedataObject>() {
            @Override
            public void onResponse(Call<MovieImagedataObject> call, Response<MovieImagedataObject> response) {
                if (response.code() == 429) {
                    mNextCallPossibleIn = Integer.parseInt(response.raw().header("Retry-After"));
                    mAvailableRequests = 0;
                    initTimer(mTmdbService.getMovieImages(movieId, TmdbRestConfig.API_KEY), listener, movieId);
                } else if (response.code() == 200) {
                    if (response.body() != null) {
                        mAvailableRequests =  Integer.parseInt(response.raw().header("X-RateLimit-Remaining"));
                        listener.onImagesLoaded(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieImagedataObject> call, Throwable t) {
                t.printStackTrace();
            }
        };
    }

    /**
     * clear the pool to make room for new requests
     */
    public void clearPool() {
        for (int i = 0; i < mRequestPool.size(); ++i) {
            mRequestPool.get(i).getmPooledCall().cancel();
        }
        mRequestPool.clear();
    }

    /**
     * initialize the timer used to dispatch the pooled calls when the TMDB API limit has been reached
     * waits till the API cool down has been completed
     * @param call the call to enqueue if no calls have been pooled
     * @param listener the listener to be called when no calls have been pooled
     * @param movieId the calls movieId
     */
   private void initTimer(@NonNull final Call<MovieImagedataObject> call ,@NonNull final TmdbLoadListener listener, @NonNull final Long movieId) {
       if (mTimer != null) {
           mTimer.cancel();
       }
       Log.d(getClass().getName(), "Timer init!");
       mTimer = new CountDownTimer(mNextCallPossibleIn * 1000, 1000) {
           @Override
           public void onTick(long millisUntilFinished) { }

           @Override
           public void onFinish() {
               Log.d(getClass().getName(), "Timer finished");
               mAvailableRequests = mMaxRequests;
               if (mRequestPool.size() > 0) {
                   RequestPoolObject poolObj;
                   for (int i = 0; i < mRequestPool.size(); ++i) {
                       poolObj = mRequestPool.get(i);
                       poolObj.getmPooledCall().enqueue(getMovieImageCallback(poolObj.getmPooledListener(), poolObj.getmPooledMovieId()));
                   }
                   mRequestPool.clear();
               } else {
                   call.enqueue(getMovieImageCallback(listener, movieId));
               }
               mTimer = null;
           }

       };
       mTimer.start();
   }
}
