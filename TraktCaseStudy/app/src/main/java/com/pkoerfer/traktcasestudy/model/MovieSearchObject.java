package com.pkoerfer.traktcasestudy.model;

import com.google.gson.annotations.SerializedName;
import com.pkoerfer.traktcasestudy.util.Loca;

/**
 * Created by Pascal on 14.10.2016.
 */

public class MovieSearchObject extends BaseSearchObject{

    @SerializedName("movie")
    private MovieDataObject movie;

    public MovieSearchObject(String type, Double score, MovieDataObject movie) {
        super(type, score);
        this.movie = movie;
    }

    public MovieDataObject getMovie() {
        return movie;
    }
}
