package com.pkoerfer.traktcasestudy.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pkoerfer.traktcasestudy.R;
import com.pkoerfer.traktcasestudy.model.MovieSearchObject;
import com.pkoerfer.traktcasestudy.rest.RestHandler;
import com.pkoerfer.traktcasestudy.rest.movieDatabase.TmdbRestHandler;
import com.pkoerfer.traktcasestudy.ui.adapter.QueryResultAdapter;
import com.pkoerfer.traktcasestudy.util.SearchTermListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * class responsible for displaying the search {@link Fragment}
 * responsible for holding the views and listens for new data
 * Created by Pascal on 13.10.2016.
 */
public class SearchFragment extends Fragment {

    @BindView(R.id.search_editText)
    EditText mSearchET;

    @BindView(R.id.search_result_recyclerView)
    RecyclerView mResultRV;

    @BindView(R.id.search_loading_container)
    View mLoadingContainer;

    private GridLayoutManager mResultGridLayoutManager;
    private QueryResultAdapter mResultAdapter;
    private RecyclerView.OnScrollListener mResultScrollListener;

    private TextWatcher mSearchTextWatcher;
    private SearchTermListener mSearchTermListener;

    private int mNextPageToLoad = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TmdbRestHandler.getInstance().init();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fr_search, null, false);
        ButterKnife.bind(this, mainView);
        mSearchET.addTextChangedListener(getSearchTextWatcher());
        mSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

        mResultGridLayoutManager = new GridLayoutManager(getContext(), 2);
        mResultRV.setLayoutManager(mResultGridLayoutManager);
        mResultRV.addItemDecoration(new ListItemDecorator());
        mResultRV.addOnScrollListener(getScrollChangeListener());
        mResultAdapter = new QueryResultAdapter();
        mResultRV.setAdapter(mResultAdapter);
        return mainView;
    }

    /**
     * create a {@link android.support.v7.widget.RecyclerView.OnScrollListener}
     * listener forces new drawing of displayed items after scrolling
     * if the lists and is reached new Items are loaded
     * @return the created OnScrollListener
     */
    private RecyclerView.OnScrollListener getScrollChangeListener() {
        if (mResultScrollListener == null) {
            mResultScrollListener = new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        if (mResultGridLayoutManager.findLastVisibleItemPosition() == mResultAdapter.getItemCount() - 1) {
                            mLoadingContainer.setVisibility(View.VISIBLE);
                            RestHandler.getInstance().getQueryResults(getSearchTermListener(), mSearchET.getText().toString(), mNextPageToLoad);
                        }

                        int relayoutFromPos = mResultGridLayoutManager.findFirstVisibleItemPosition();
                        int relayoutToPos = mResultGridLayoutManager.findLastVisibleItemPosition();

                        TmdbRestHandler.getInstance().clearPool();
                        for (int i = relayoutFromPos; i <= relayoutToPos; ++i) {
                            mResultAdapter.notifyItemChanged(i);
                        }
                    }
                }
            };
        }
        return mResultScrollListener;
    }

    /***
     * creates a {@link TextWatcher}
     * clears the current search results when a change was made
     * dispatches loading of new query for text fields content
     * @return the created TextWatcher
     */
    private TextWatcher getSearchTextWatcher() {
        if (mSearchTextWatcher == null) {
            mSearchTextWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mNextPageToLoad = 1;
                    mResultAdapter.clear();
                    TmdbRestHandler.getInstance().clearPool();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    mLoadingContainer.setVisibility(View.VISIBLE);
                    RestHandler.getInstance().getQueryResults(getSearchTermListener(), s.toString(), 1);
                }
            };
        }
        return mSearchTextWatcher;
    }

    /**
     * creates a callback listener for movie queries
     * updates the {@link RecyclerView} Adapter with new data
     * @return the created listener
     */
    private SearchTermListener getSearchTermListener() {
        if (mSearchTermListener == null) {
            mSearchTermListener = new SearchTermListener() {
                @Override
                public void onSearchResultReceived(@NonNull List<MovieSearchObject> movies, @NonNull Integer currentPage) {
                    mLoadingContainer.setVisibility(View.GONE);
                    mNextPageToLoad = currentPage + 1;
                    mResultAdapter.setData(movies);
                }
            };
        }
        return mSearchTermListener;
    }
}
