package com.pkoerfer.traktcasestudy.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pkoerfer.traktcasestudy.model.MovieDataObject;
import com.pkoerfer.traktcasestudy.R;
import com.pkoerfer.traktcasestudy.rest.RestConfig;
import com.pkoerfer.traktcasestudy.rest.RestHandler;
import com.pkoerfer.traktcasestudy.ui.adapter.MovieAdapter;
import com.pkoerfer.traktcasestudy.util.LoadMoviesListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * class responsible for displaying the popular movies {@link Fragment}
 * responsible for view holding and data loading
 * Created by Pascal on 13.10.2016.
 */
public class MovieFragment extends Fragment {

    @BindView(R.id.movie_recyclerView)
    RecyclerView mMovieRV;

    @BindView(R.id.movie_loading_container)
    View mLoadingContainer;

    private LinearLayoutManager mMovieLayoutManager;
    private MovieAdapter mMovieAdapter;
    private RecyclerView.OnScrollListener mMovieScrollListener;

    private LoadMoviesListener mLoadMoviesListener;

    private int mNextPageToLoad = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RestConfig conf = new RestConfig();
        RestHandler.getInstance().init(conf);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fr_movie, null, false);
        ButterKnife.bind(this, mainView);

        mMovieAdapter = new MovieAdapter();
        mMovieLayoutManager = new LinearLayoutManager(getContext());
        mMovieRV.setLayoutManager(mMovieLayoutManager);
        mMovieRV.setAdapter(mMovieAdapter);
        mMovieRV.addItemDecoration(new ListItemDecorator());
        mMovieRV.addOnScrollListener(getMovieScrollListener());
        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoadingContainer.setVisibility(View.VISIBLE);
        RestHandler.getInstance().getPopularMovies(getLoadMoviesListener(), mNextPageToLoad);
    }

    private RecyclerView.OnScrollListener getMovieScrollListener() {
        if (mMovieScrollListener == null) {
            mMovieScrollListener = new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        if (mMovieLayoutManager.findLastVisibleItemPosition() == mMovieAdapter.getItemCount() - 1) {
                            mLoadingContainer.setVisibility(View.VISIBLE);
                            RestHandler.getInstance().getPopularMovies(getLoadMoviesListener(), mNextPageToLoad);
                        }
                    }
                }
            };
        }
        return mMovieScrollListener;
    }

    private LoadMoviesListener getLoadMoviesListener() {
        if (mLoadMoviesListener == null) {
            mLoadMoviesListener = new LoadMoviesListener() {
                @Override
                public void onLoadingFinished(List<MovieDataObject> movies, Integer currentPage) {
                    mLoadingContainer.setVisibility(View.GONE);
                    mNextPageToLoad = currentPage + 1;
                    mMovieAdapter.addMovies(movies);
                }
            };
        }
        return mLoadMoviesListener;
    }
}
