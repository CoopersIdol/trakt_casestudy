package com.pkoerfer.traktcasestudy.rest.movieDatabase;

import com.pkoerfer.traktcasestudy.model.tmdb.TmdbApiConfiguration;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * class responsible for creating the TMDB retrofit configuration
 * Created by Pascal on 14.10.2016.
 */
public class TmdbRestConfig {

    private static TmdbRestConfig instance;

    private Retrofit mRetrofit;
    private TmdbApiConfiguration mApiConfiguration;

    public static final String API_KEY = "7283b3331d2cb69daedb87f6509b4489";

    private TmdbRestConfig() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static TmdbRestConfig getInstance() {
        if (instance == null) {
            instance = new TmdbRestConfig();
        }
        return instance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public TmdbApiConfiguration getApiConfiguration() {
        return mApiConfiguration;
    }

    public void setApiConfiguration(TmdbApiConfiguration apiConfiguration) {
        this.mApiConfiguration = apiConfiguration;
    }
}
