package com.pkoerfer.traktcasestudy;

import com.pkoerfer.traktcasestudy.model.tmdb.MovieImagedataObject;
import com.pkoerfer.traktcasestudy.util.TmdbLoadListener;

import retrofit2.Call;

/**
 * class representing a request pool object
 * used to store TMDb-Request which could not be dispatched
 * needed because a API Limit of 40 requests/sec exists
 * Created by Pascal on 15.10.2016.
 */
public class RequestPoolObject {
    private Call<MovieImagedataObject> mPooledCall = null;
    private TmdbLoadListener mPooledListener;
    private Long mPooledMovieId;

    public RequestPoolObject(Call<MovieImagedataObject> mPooledCall, TmdbLoadListener mPooledListener, Long mPooledMovieId) {
        this.mPooledCall = mPooledCall;
        this.mPooledListener = mPooledListener;
        this.mPooledMovieId = mPooledMovieId;
    }

    public Call<MovieImagedataObject> getmPooledCall() {
        return mPooledCall;
    }

    public TmdbLoadListener getmPooledListener() {
        return mPooledListener;
    }

    public Long getmPooledMovieId() {
        return mPooledMovieId;
    }
}
