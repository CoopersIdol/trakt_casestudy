package com.pkoerfer.traktcasestudy.rest.movieDatabase;

import com.pkoerfer.traktcasestudy.model.tmdb.MovieImagedataObject;
import com.pkoerfer.traktcasestudy.model.tmdb.TmdbApiConfiguration;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * interface containing the calls made to the TMDB API
 * Created by Pascal on 14.10.2016.
 */
public interface TmdbService {
    @GET("configuration")
    Call<TmdbApiConfiguration> getApiConfiguration(@Query("api_key") String apiKey);

    @GET("movie/{movieId}/images")
    Call<MovieImagedataObject> getMovieImages(@Path("movieId") Long movieId, @Query("api_key") String apiKey);
}
