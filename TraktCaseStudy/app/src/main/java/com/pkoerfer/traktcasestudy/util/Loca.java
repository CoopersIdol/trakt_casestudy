package com.pkoerfer.traktcasestudy.util;

import android.content.Context;

/**
 * class used to retrieve strings formatted with given parameter(s)
 * Created by Pascal on 13.10.2016.
 */
public class Loca {
    public static String getStringWithParamter(Context context, int resourceId, Object... args) {
        if (args != null) {
            return String.format(context.getText(resourceId).toString(), args);
        } else {
            return context.getText(resourceId).toString();
        }
    }
}
