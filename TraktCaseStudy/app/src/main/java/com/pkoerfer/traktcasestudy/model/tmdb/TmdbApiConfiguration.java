package com.pkoerfer.traktcasestudy.model.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 15.10.2016.
 */

public class TmdbApiConfiguration {

    @SerializedName("images")
    private TmdbImagesDataObject images;

    @SerializedName("change_keys")
    private String[] changeKeys;

    public TmdbApiConfiguration(TmdbImagesDataObject images, String[] changeKeys) {
        this.images = images;
        this.changeKeys = changeKeys;
    }

    public TmdbImagesDataObject getImages() {
        return images;
    }

    public String[] getChangeKeys() {
        return changeKeys;
    }
}
