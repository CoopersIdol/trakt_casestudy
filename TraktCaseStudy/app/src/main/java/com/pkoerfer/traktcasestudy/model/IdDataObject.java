package com.pkoerfer.traktcasestudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 13.10.2016.
 */

public class IdDataObject {

    @SerializedName("trakt")
    private Long traktId;

    @SerializedName("slug")
    private String slug;

    @SerializedName("imdb")
    private String imdbId;

    @SerializedName("tmdb")
    private Long tmdbId;

    public IdDataObject(Long traktId, String slug, String imdbId, Long tmdbid) {
        this.traktId = traktId;
        this.slug = slug;
        this.imdbId = imdbId;
        this.tmdbId = tmdbid;
    }

    //<editor-fold desc="Getter && Setter">
    public Long getTraktId() {
        return traktId;
    }

    public String getSlug() {
        return slug;
    }

    public String getImdbId() {
        return imdbId;
    }

    public Long getTmdbId() {
        return tmdbId;
    }
    //</editor-fold>
}
