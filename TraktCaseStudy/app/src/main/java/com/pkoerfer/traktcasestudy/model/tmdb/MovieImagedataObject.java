package com.pkoerfer.traktcasestudy.model.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 15.10.2016.
 */

public class MovieImagedataObject {

    @SerializedName("id")
    private Long id;

    @SerializedName("backdrops")
    private ImageDataObject[] backdrops;

    @SerializedName("posters")
    private ImageDataObject[] posters;

    public MovieImagedataObject(Long id, ImageDataObject[] backdrops, ImageDataObject[] posters) {
        this.id = id;
        this.backdrops = backdrops;
        this.posters = posters;
    }

    public Long getId() {
        return id;
    }

    public ImageDataObject[] getBackdrops() {
        return backdrops;
    }

    public ImageDataObject[] getPosters() {
        return posters;
    }
}
