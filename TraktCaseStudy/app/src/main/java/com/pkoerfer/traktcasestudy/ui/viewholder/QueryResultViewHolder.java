package com.pkoerfer.traktcasestudy.ui.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pkoerfer.traktcasestudy.R;
import com.pkoerfer.traktcasestudy.model.MovieSearchObject;
import com.pkoerfer.traktcasestudy.model.tmdb.MovieImagedataObject;
import com.pkoerfer.traktcasestudy.rest.movieDatabase.TmdbRestConfig;
import com.pkoerfer.traktcasestudy.rest.movieDatabase.TmdbRestHandler;
import com.pkoerfer.traktcasestudy.util.TmdbLoadListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * class responsible for displaying query results
 * dispatches loading of movie image and displaying it
 * Created by Pascal on 14.10.2016.
 */
public class QueryResultViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.resultCell_title_textView)
    TextView mTitleTV;

    @BindView(R.id.resultCell_movie_imageView)
    ImageView mMovieImage;

    @BindView(R.id.resultCell_year_textView)
    TextView mYearTV;

    private TmdbLoadListener mLoadListener;

    public QueryResultViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    /**
     * display the given movies data
     * dispatch loading of movies image configuration
     * @param movie to be displayed
     */
    public void displayMovieOverview(MovieSearchObject movie) {
        Log.d(getClass().getName(), movie.getMovie().toString());
        Glide.clear(mMovieImage);
        mMovieImage.setImageResource(R.drawable.ic_movie_black_48dp);
        mYearTV.setVisibility(View.INVISIBLE);
        if (movie != null) {
            mTitleTV.setText(movie.getMovie().getTitle());
            if (movie.getMovie().getYear() != null) {
                mYearTV.setText(movie.getMovie().getYear().toString());
                mYearTV.setVisibility(View.VISIBLE);
            }
            if (movie.getMovie().getIds().getTmdbId() != null) {
                TmdbRestHandler.getInstance().getMovieImages(getLoadListener(), movie.getMovie().getIds().getTmdbId());
            }
        }
    }

    /**
     * create a callback listener for movie image configuration
     * dispatch loading of the movie Image
     * @return the created callback
     */
    private TmdbLoadListener getLoadListener() {
        if (mLoadListener == null) {
            mLoadListener = new TmdbLoadListener() {
                @Override
                public void onImagesLoaded(@NonNull MovieImagedataObject images) {
                    mMovieImage.setImageResource(R.drawable.ic_movie_black_48dp);
                    SpannableStringBuilder uri = new SpannableStringBuilder(TmdbRestConfig.getInstance().getApiConfiguration().getImages().getSecureBaseUrl());
                    if (images.getPosters().length > 0) {
                        uri.append(TmdbRestConfig.getInstance().getApiConfiguration().getImages().getBackdropSizes()[0])
                                .append("/")
                                .append(images.getPosters()[0].getFilePath());
                        Glide.with(mMovieImage.getContext()).load(uri.toString()).placeholder(R.drawable.ic_movie_black_48dp).into(mMovieImage);
                    } else if (images.getBackdrops().length > 0) {
                        uri.append(TmdbRestConfig.getInstance().getApiConfiguration().getImages().getBackdropSizes()[0])
                                .append("/")
                                .append(images.getBackdrops()[0].getFilePath());
                        Glide.with(mMovieImage.getContext()).load(uri.toString()).placeholder(R.drawable.ic_movie_black_48dp).into(mMovieImage);
                    }
                }
            };
        }
        return mLoadListener;
    }
}
