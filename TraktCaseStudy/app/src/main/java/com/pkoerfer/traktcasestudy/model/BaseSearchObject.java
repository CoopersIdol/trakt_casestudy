package com.pkoerfer.traktcasestudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pascal on 14.10.2016.
 */

public class BaseSearchObject {

    @SerializedName("type")
    private String type;

    @SerializedName("score")
    private Double score;

    public BaseSearchObject(String type, Double score) {
        this.type = type;
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public Double getScore() {
        return score;
    }
}
